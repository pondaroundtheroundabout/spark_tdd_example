import unittest2
from sparktestingbase.sqltestcase import SQLTestCase
from pyspark.sql.functions import row_number, lit

import glassbox_based_active_time_calculator as code

from pyspark.sql.types import StructType, StructField, StringType, IntegerType, TimestampType, LongType


# The base schema that is reused all over this test file
required_glassbox_event_fields_schema = StructType([
    StructField(code.FIELD__USER_ID, IntegerType(), True),
    StructField(code.FIELD__CLIENT_TIME, TimestampType(), True),
    StructField(code.FIELD__SERVER_TIME, TimestampType(), True),
    StructField(code.FIELD__NETWORK_STATUS, StringType(), True),
    StructField(code.FIELD__PRODUCT_NAME, StringType(), True)
])

# Extra fields for the base schema
struct_field__client_server_diff__all = StructField(code.FIELD__CLIENT_SERVER_DIFF__ALL, LongType(), True)
struct_field__client_server_diff__online = StructField(code.FIELD__CLIENT_SERVER_DIFF__ONLINE, LongType(), True)
struct_field__correction_partition_value = StructField(code.FIELD__CORRECTION_PARTITION_VALUE, LongType(), False)


# These indexes can be used to select `required_glassbox_event_fields_schema` easier
SCHEMA_FIELD_INDEX__USER_ID = 0
SCHEMA_FIELD_INDEX__CLIENT_TIME = 1
SCHEMA_FIELD_INDEX__SERVER_TIME = 2
SCHEMA_FIELD_INDEX__NETWORK_STATUS = 3
SCHEMA_FIELD_INDEX__PRODUCT_NAME = 4


class TestProductNameHandlingHistorical(SQLTestCase):
    """
        The `product_name` column is being added to the source data. Historical records don't have it.
        This class tests `handle_default_product_name()` function for the old sources not containing the column.
    """

    def setUp(self):

        super(TestProductNameHandlingHistorical, self).setUp()

        """ Source DataFrame that doesn't contain `product_name`. """
        no_product_name_column_schema = StructType([
            StructField("foo_id", StringType(), True)
        ])

        no_product_name_column_json = ['{"foo_id": "01"}']

        self.no_product_name_column_df = self.sqlCtx.read.json(
            self.sc.parallelize(no_product_name_column_json),
            schema=no_product_name_column_schema
        )

        """ Expected DataFrame schema. """
        self.with_product_name_column_schema = StructType([
            StructField("foo_id", StringType(), True),
            StructField(code.FIELD__PRODUCT_NAME, StringType(), False)
        ])

    def _test_handle_default_product_name(self, param=code.DEFAULT_VALUE__PRODUCT_NAME):
        """ Helper function to test the function on a DataFrame not containing the column. """

        with_product_name_column_json = ['{"foo_id": "01", "' + code.FIELD__PRODUCT_NAME + '": "%s"}' % param]

        expected_df = self.sqlCtx.read.json(
            self.sc.parallelize(with_product_name_column_json),
            schema=self.with_product_name_column_schema
        )

        if param == code.DEFAULT_VALUE__PRODUCT_NAME:     # Use the default value

            with_product_name_df = code.GlassboxBasedActiveTimeCalculator().handle_default_product_name(
                self.no_product_name_column_df
            )

        else:   # Pass the requested parameter instead of default value

            with_product_name_df = code.GlassboxBasedActiveTimeCalculator().handle_default_product_name(
                self.no_product_name_column_df, param
            )

        # Make sure we get back the expected DataFrame
        self.assertEqual(self.with_product_name_column_schema, with_product_name_df.schema)

        # Extra check. Make sure the schemas are equal. Note: False nullability for `product_name`.
        self.assertDataFrameEqual(expected_df, with_product_name_df)

    def test_with_default_value(self):
        """ Test the function with no parameter.
        Expecting default value: 'Next'. """
        self._test_handle_default_product_name()

    def test_with_parameter_value(self):
        """ Test the function with parameter.
        Expected value: 'PARAMETER'. """
        self._test_handle_default_product_name("PARAMETER")


class TestProductNameHandlingFuture(SQLTestCase):
    """
        The `product_name` column is being added to the source data.
        Historical records don't have it and future records will have it.
        This class tests `handle_default_product_name()` function for the new sources containing the column.
    """

    def setUp(self):

        super(TestProductNameHandlingFuture, self).setUp()

        with_product_name_column_schema = StructType([
            StructField("foo_id", StringType(), True),
            StructField(code.FIELD__PRODUCT_NAME, StringType(), True)
        ])

        # Note: `product_name`: null should change to 'Next'.
        with_product_name_column_json = [
            '{"foo_id": "01", "' + code.FIELD__PRODUCT_NAME + '": null}',
            '{"foo_id": "02", "' + code.FIELD__PRODUCT_NAME + '": "Next"}',
            '{"foo_id": "03", "' + code.FIELD__PRODUCT_NAME + '": "Video"}',
        ]

        self.source_df = self.sqlCtx.read.json(
            self.sc.parallelize(with_product_name_column_json),
            schema=with_product_name_column_schema
        )

        expected_json = [
            '{"foo_id": "01", "' + code.FIELD__PRODUCT_NAME + '": "Next"}',
            '{"foo_id": "02", "' + code.FIELD__PRODUCT_NAME + '": "Next"}',
            '{"foo_id": "03", "' + code.FIELD__PRODUCT_NAME + '": "Video"}',
        ]

        self.expected_df = self.sqlCtx.read.json(
            self.sc.parallelize(expected_json),
            schema=with_product_name_column_schema
        )

    def test_handle_default_product_name(self):
        """ Make sure the function that replaces `null` values with default ('Next') is working correct.
        Expecting foo_id=1 to change to default 'Next'. """

        result_df = code.GlassboxBasedActiveTimeCalculator().handle_default_product_name(self.source_df)

        self.assertDataFrameEqual(self.expected_df, result_df)


class TestGlassboxBasedActiveTimeCalculatorConstructor(SQLTestCase):
    """ Test the constructor for GlassboxBasedActiveTimeCalculator. """

    def setUp(self):

        super(TestGlassboxBasedActiveTimeCalculatorConstructor, self).setUp()

        # Note: `product_name`: null should change to 'Next' and `extra_column` should not be selected.
        source_json = [
            '{"user__user_id": 1, "' + code.FIELD__PRODUCT_NAME + '": null, "extra_column":"should_not_be_selected", "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:00.001", "' + code.FIELD__SERVER_TIME + '":"2018-05-13 00:00:00.001", "' + code.FIELD__NETWORK_STATUS + '": "not_important"}',
            '{"user__user_id": 2, "' + code.FIELD__PRODUCT_NAME + '": "Next", "extra_column":"should_not_be_selected", "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:00.001", "' + code.FIELD__SERVER_TIME + '":"2018-05-13 00:00:00.001", "' + code.FIELD__NETWORK_STATUS + '": "not_important"}',
            '{"user__user_id": 3, "' + code.FIELD__PRODUCT_NAME + '": "Video", "extra_column":"should_not_be_selected", "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:00.001", "' + code.FIELD__SERVER_TIME + '":"2018-05-13 00:00:00.001", "' + code.FIELD__NETWORK_STATUS + '": "not_important"}',
        ]

        # Append an extra column to `required_glassbox_event_fields_schema` to make sure the constructor removes it.
        extra_column_to_be_appended = [StructField("extra_column", StringType(), True)]
        schema_with_extra_field = StructType(required_glassbox_event_fields_schema.fields + extra_column_to_be_appended)

        self.source_df = self.sqlCtx.read.json(
            self.sc.parallelize(source_json),
            schema=schema_with_extra_field
        )

        expected_json = [
            '{"user__user_id": 1, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:00.001", "' + code.FIELD__SERVER_TIME + '":"2018-05-13 00:00:00.001", "' + code.FIELD__NETWORK_STATUS + '": "not_important"}',
            '{"user__user_id": 2, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:00.001", "' + code.FIELD__SERVER_TIME + '":"2018-05-13 00:00:00.001", "' + code.FIELD__NETWORK_STATUS + '": "not_important"}',
            '{"user__user_id": 3, "' + code.FIELD__PRODUCT_NAME + '": "Video", "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:00.001", "' + code.FIELD__SERVER_TIME + '":"2018-05-13 00:00:00.001", "' + code.FIELD__NETWORK_STATUS + '": "not_important"}',
        ]

        self.expected_df = self.sqlCtx.read.json(
            self.sc.parallelize(expected_json),
            schema=required_glassbox_event_fields_schema
        )

    def test_constructor(self):
        """ Make sure the constructor of GlassboxBasedActiveTimeCalculator replaces `null` values with default ('Next')
        and selecting only required columns.
        Also make sure ct__source_rows countains the correct count for the source data. """

        calculator = code.GlassboxBasedActiveTimeCalculator(self.source_df)

        result_df = calculator.required_columns_df

        self.assertDataFrameEqual(self.expected_df, result_df)
        self.assertEqual(3, calculator.ct__source_rows)

    def test_constructor_on_prod_like_data(self):
        """ Test the constructor on production like data.
        Note: the schema is a bit different that it was used in previous tests.
        Make sure correct count is recorded. """

        source_df = self.sqlCtx.read.orc("production_input/*")

        # Change the nullability of `product_name` to False.
        expected_schema = StructType(
            required_glassbox_event_fields_schema.fields[:-1]
            + [StructField(code.FIELD__PRODUCT_NAME, StringType(), False)]
        )

        calculator = code.GlassboxBasedActiveTimeCalculator(source_df)

        self.assertEqual(expected_schema, calculator.required_columns_df.schema)
        self.assertEqual(1051084, calculator.ct__source_rows)

        # Here is an opportunity to gather some production like data (note: only required columns)
        # Just comment out lines below.
        #
        # df = calculator.required_columns_df
        # df.show(20, False)
        # df.filter(df["user__user_id"] == lit(253029024)).show(20, False)

    def test_constructor_window(self):
        """ Test if the window on `user__user_id` ordered by `core__server_time` is correct. """

        # Two users with opposite time ordering. One of them have 2 different product usage
        source_json = [
            '{"user__user_id": 1, "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:01.2", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 00:00:01.2", "' + code.FIELD__NETWORK_STATUS + '": "not_important", "' + code.FIELD__PRODUCT_NAME + '": "Next"}',
            '{"user__user_id": 1, "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:01.1", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 00:00:01.1", "' + code.FIELD__NETWORK_STATUS + '": "not_important", "' + code.FIELD__PRODUCT_NAME + '": "Next"}',

            '{"user__user_id": 2, "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:01.2", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 00:00:01.2", "' + code.FIELD__NETWORK_STATUS + '": "not_important", "' + code.FIELD__PRODUCT_NAME + '": "Next"}',
            '{"user__user_id": 2, "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:01.1", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 00:00:01.1", "' + code.FIELD__NETWORK_STATUS + '": "not_important", "' + code.FIELD__PRODUCT_NAME + '": "Next"}',

            '{"user__user_id": 1, "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:01.2", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 00:00:01.2", "' + code.FIELD__NETWORK_STATUS + '": "not_important", "' + code.FIELD__PRODUCT_NAME + '": "Video"}',
            '{"user__user_id": 1, "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:01.1", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 00:00:01.1", "' + code.FIELD__NETWORK_STATUS + '": "not_important", "' + code.FIELD__PRODUCT_NAME + '": "Video"}',
        ]
        source_df = self.sqlCtx.read.json(
            self.sc.parallelize(source_json),
            schema=required_glassbox_event_fields_schema
        )

        # Make sure ordering is fixed, grouping by `user__user_id` and `product_type` restarts counter
        expected_json = [
            '{"user__user_id": 1, "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:01.1", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 00:00:01.1", "' + code.FIELD__NETWORK_STATUS + '": "not_important", "' + code.FIELD__PRODUCT_NAME + '": "Video", "row_number": 1}',
            '{"user__user_id": 1, "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:01.2", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 00:00:01.2", "' + code.FIELD__NETWORK_STATUS + '": "not_important", "' + code.FIELD__PRODUCT_NAME + '": "Video", "row_number": 2}',

            '{"user__user_id": 1, "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:01.1", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 00:00:01.1", "' + code.FIELD__NETWORK_STATUS + '": "not_important", "' + code.FIELD__PRODUCT_NAME + '": "Next", "row_number": 1}',
            '{"user__user_id": 1, "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:01.2", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 00:00:01.2", "' + code.FIELD__NETWORK_STATUS + '": "not_important", "' + code.FIELD__PRODUCT_NAME + '": "Next", "row_number": 2}',

            '{"user__user_id": 2, "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:01.1", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 00:00:01.1", "' + code.FIELD__NETWORK_STATUS + '": "not_important", "' + code.FIELD__PRODUCT_NAME + '": "Next", "row_number": 1}',
            '{"user__user_id": 2, "' + code.FIELD__CLIENT_TIME + '": "2018-05-13 00:00:01.2", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 00:00:01.2", "' + code.FIELD__NETWORK_STATUS + '": "not_important", "' + code.FIELD__PRODUCT_NAME + '": "Next", "row_number": 2}',
        ]

        # Add an extra `row_number` column.
        expected_schema = StructType(
            required_glassbox_event_fields_schema.fields + [StructField("row_number", IntegerType(), True)]
        )

        expected_df = self.sqlCtx.read.json(
            self.sc.parallelize(expected_json),
            schema=expected_schema
        )

        calculator = code.GlassboxBasedActiveTimeCalculator(source_df)
        windowed_df = calculator.required_columns_df.withColumn("row_number", row_number().over(calculator.window))

        self.assertDataFrameEqual(expected_df, windowed_df)


class TestSettingTimeDifferences(SQLTestCase):

    def setUp(self):

        super(TestSettingTimeDifferences, self).setUp()

        self.without_difference_calculated_json = [

            # less than 1 sec difference positive
            '{"user__user_id": 1, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:01.000", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 01:00:01.001"}',
            '{"user__user_id": 1, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:01.000", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 01:00:01.010"}',
            '{"user__user_id": 1, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:01.000", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 01:00:01.100"}',
            '{"user__user_id": 1, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:01.000", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 01:00:01.999"}',

            # less than 1 sec difference negative
            '{"user__user_id": 2, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 02:00:02.001", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 02:00:02.000"}',
            '{"user__user_id": 2, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 02:00:02.010", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 02:00:02.000"}',
            '{"user__user_id": 2, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 02:00:02.100", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 02:00:02.000"}',
            '{"user__user_id": 2, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 02:00:02.999", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 02:00:02.000"}',

            # exactly 1 sec positive
            '{"user__user_id": 3, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 03:00:04", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 03:00:03"}',
            '{"user__user_id": 3, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 03:00:05.000", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 03:00:04.000"}',
            '{"user__user_id": 3, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 03:00:05.001", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 03:00:04.001"}',

            # exactly 1 sec negative
            '{"user__user_id": 4, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 04:00:03", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 04:00:04"}',
            '{"user__user_id": 4, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 04:00:04.000", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 04:00:05.000"}',
            '{"user__user_id": 4, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 04:00:04.001", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 04:00:05.001"}',

            # around 1 day positive
            '{"user__user_id": 5, "' + code.FIELD__CLIENT_TIME + '": "2019-05-14 05:00:05", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 05:00:04.999"}',
            '{"user__user_id": 5, "' + code.FIELD__CLIENT_TIME + '": "2019-05-14 05:00:05.000", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 05:00:05.000"}',
            '{"user__user_id": 5, "' + code.FIELD__CLIENT_TIME + '": "2019-05-14 05:00:05.000", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 05:00:05.001"}',
            '{"user__user_id": 5, "' + code.FIELD__CLIENT_TIME + '": "2019-05-14 05:00:05.000", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 05:00:05.999"}',
            '{"user__user_id": 5, "' + code.FIELD__CLIENT_TIME + '": "2019-05-14 05:00:05.000", "' + code.FIELD__SERVER_TIME + '": "2019-05-13 05:00:06"}',

            # around 1 day negative
            '{"user__user_id": 6, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 06:00:04.999", "' + code.FIELD__SERVER_TIME + '": "2019-05-14 06:00:05"}',
            '{"user__user_id": 6, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 06:00:05.000", "' + code.FIELD__SERVER_TIME + '": "2019-05-14 06:00:05.000"}',
            '{"user__user_id": 6, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 06:00:05.001", "' + code.FIELD__SERVER_TIME + '": "2019-05-14 06:00:05.000"}',
            '{"user__user_id": 6, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 06:00:05.999", "' + code.FIELD__SERVER_TIME + '": "2019-05-14 06:00:05.000"}',
            '{"user__user_id": 6, "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 06:00:06", "' + code.FIELD__SERVER_TIME + '": "2019-05-14 06:00:05.000"}',
        ]

        # This DataFrame is used both as expected and as source.
        # Based on it's purpose we will select different columns in the tests.
        self.difference_calculated_for_all_rows_json = [

            # Less than 1 sec positive
            '{"user__user_id": 1, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:01.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 01:00:01.001", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 0, "' + code.FIELD__NETWORK_STATUS + '": null, "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": 0}',
            '{"user__user_id": 1, "' + code.FIELD__PRODUCT_NAME + '": "Video", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:01.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 01:00:01.010", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 0, "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": 0}',
            '{"user__user_id": 1, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:01.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 01:00:01.100", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 0, "' + code.FIELD__NETWORK_STATUS + '": "OFFLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": null}',
            '{"user__user_id": 1, "' + code.FIELD__PRODUCT_NAME + '": "Video", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:01.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 01:00:01.999", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 0, "' + code.FIELD__NETWORK_STATUS + '": null, "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": 0}',

            # Less than 1 sec negative
            '{"user__user_id": 2, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 02:00:02.001", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 02:00:02.000", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 0, "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": 0}',
            '{"user__user_id": 2, "' + code.FIELD__PRODUCT_NAME + '": "Video", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 02:00:02.010", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 02:00:02.000", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 0, "' + code.FIELD__NETWORK_STATUS + '": "OFFLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": null}',
            '{"user__user_id": 2, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 02:00:02.100", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 02:00:02.000", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 0, "' + code.FIELD__NETWORK_STATUS + '": null, "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": 0}',
            '{"user__user_id": 2, "' + code.FIELD__PRODUCT_NAME + '": "Video", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 02:00:02.999", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 02:00:02.000", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 0, "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": 0}',

            # exactly 1 sec positive
            '{"user__user_id": 3, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 03:00:04", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 03:00:03", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 1, "' + code.FIELD__NETWORK_STATUS + '": "OFFLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": null}',
            '{"user__user_id": 3, "' + code.FIELD__PRODUCT_NAME + '": "Video", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 03:00:05.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 03:00:04.000", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 1, "' + code.FIELD__NETWORK_STATUS + '": null, "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": 1}',
            '{"user__user_id": 3, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 03:00:05.001", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 03:00:04.001", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 1, "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": 1}',

            # exactly 1 sec negative
            '{"user__user_id": 4, "' + code.FIELD__PRODUCT_NAME + '": "Video", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 04:00:03", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 04:00:04", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": -1, "' + code.FIELD__NETWORK_STATUS + '": "OFFLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": null}',
            '{"user__user_id": 4, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 04:00:04.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 04:00:05.000", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": -1, "' + code.FIELD__NETWORK_STATUS + '": null, "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": -1}',
            '{"user__user_id": 4, "' + code.FIELD__PRODUCT_NAME + '": "Video", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 04:00:04.001", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 04:00:05.001", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": -1, "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": -1}',

            # around 1 day positive
            '{"user__user_id": 5, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-14 05:00:05", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 05:00:04.999", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 86401, "' + code.FIELD__NETWORK_STATUS + '": "OFFLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": null}',
            '{"user__user_id": 5, "' + code.FIELD__PRODUCT_NAME + '": "Video", "' + code.FIELD__CLIENT_TIME + '": "2019-05-14 05:00:05.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 05:00:05.000", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 86400, "' + code.FIELD__NETWORK_STATUS + '": null, "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": 86400}',
            '{"user__user_id": 5, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-14 05:00:05.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 05:00:05.001", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 86400, "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": 86400}',
            '{"user__user_id": 5, "' + code.FIELD__PRODUCT_NAME + '": "Video", "' + code.FIELD__CLIENT_TIME + '": "2019-05-14 05:00:05.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 05:00:05.999", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 86400, "' + code.FIELD__NETWORK_STATUS + '": "OFFLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": null}',
            '{"user__user_id": 5, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-14 05:00:05.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 05:00:06", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": 86399, "' + code.FIELD__NETWORK_STATUS + '": null, "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": 86399}',

            # around 1 day negative
            '{"user__user_id": 6, "' + code.FIELD__PRODUCT_NAME + '": "Video", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 06:00:04.999", "' + code.FIELD__SERVER_TIME + '":"2019-05-14 06:00:05", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": -86401, "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": -86401}',
            '{"user__user_id": 6, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 06:00:05.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-14 06:00:05.000", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": -86400, "' + code.FIELD__NETWORK_STATUS + '": "OFFLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": null}',
            '{"user__user_id": 6, "' + code.FIELD__PRODUCT_NAME + '": "Video", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 06:00:05.001", "' + code.FIELD__SERVER_TIME + '":"2019-05-14 06:00:05.000", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": -86400, "' + code.FIELD__NETWORK_STATUS + '": null, "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": -86400}',
            '{"user__user_id": 6, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 06:00:05.999", "' + code.FIELD__SERVER_TIME + '":"2019-05-14 06:00:05.000", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": -86400, "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": -86400}',
            '{"user__user_id": 6, "' + code.FIELD__PRODUCT_NAME + '": "Video", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 06:00:06", "' + code.FIELD__SERVER_TIME + '":"2019-05-14 06:00:05.000", "' + code.FIELD__CLIENT_SERVER_DIFF__ALL + '": -86399, "' + code.FIELD__NETWORK_STATUS + '": "OFFLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": null}',

        ]

        self.static_calculator = code.GlassboxBasedActiveTimeCalculator()

    def test_calculate_client_and_server_time_diff_in_seconds(self):
        """ Test if the calculation of time difference (in seconds) is correct.
        This test shows that the pyspark time difference calculation (unix_timestamp()) chops off the milliseconds.
        This is not a problem for us.
        """

        # Construct sub-schema needed to test this functionality
        source_schema = StructType(
            [required_glassbox_event_fields_schema.fields[SCHEMA_FIELD_INDEX__USER_ID]]
            + [required_glassbox_event_fields_schema.fields[SCHEMA_FIELD_INDEX__CLIENT_TIME]]
            + [required_glassbox_event_fields_schema.fields[SCHEMA_FIELD_INDEX__SERVER_TIME]]
        )

        source_df = self.sqlCtx.read.json(
            self.sc.parallelize(self.without_difference_calculated_json),
            schema=source_schema
        )

        # Add new column for the difference
        expected_schema = StructType(
            source_schema.fields + [struct_field__client_server_diff__all]
        )

        expected_df = self.sqlCtx.read.json(
            self.sc.parallelize(self.difference_calculated_for_all_rows_json),
            schema=expected_schema
        )

        result_df = self.static_calculator.calculate_client_and_server_time_diff_in_seconds(source_df)

        self.assertDataFrameEqual(expected_df, result_df)

    def test_set_client_and_server_time_diff_for_online_rows(self):
        """ Test if FIELD__CLIENT_SERVER_DIFF__ONLINE is set correctly.
        Only for rows where NETWORK_STATUS in NOT OFFLINE."""

        # Construct sub-schema needed to test this functionality
        source_schema = StructType(
            [required_glassbox_event_fields_schema.fields[SCHEMA_FIELD_INDEX__USER_ID]]
            + [required_glassbox_event_fields_schema.fields[SCHEMA_FIELD_INDEX__CLIENT_TIME]]
            + [required_glassbox_event_fields_schema.fields[SCHEMA_FIELD_INDEX__SERVER_TIME]]
            + [required_glassbox_event_fields_schema.fields[SCHEMA_FIELD_INDEX__NETWORK_STATUS]]
            + [struct_field__client_server_diff__all]
        )

        source_df = self.sqlCtx.read.json(
            self.sc.parallelize(self.difference_calculated_for_all_rows_json),
            schema=source_schema
        )

        # Add field that contains the differences for ONLINE events only
        expected_schema = StructType(
            source_schema.fields + [struct_field__client_server_diff__online]
        )

        expected_df = self.sqlCtx.read.json(
            self.sc.parallelize(self.difference_calculated_for_all_rows_json),
            schema=expected_schema
        )

        result_df = self.static_calculator.set_client_and_server_time_diff_for_online_rows(source_df)

        self.assertDataFrameEqual(expected_df, result_df)


class TestCorrectionPartition(SQLTestCase):
    """ Some users tweak their clock (or the clock gets out of sync for any reason).
    To solve this issue, we are introducing a `correction_value`
    which is equal to the last time difference we have seen when the user was online.

    In order to calculate this `correction_value` we introduce a partition field
    that only gets incremented for not 'OFFLINE' events.
    For 'OFFLINE' events it is set to the value of the last not 'OFFLINE' event's partition number.

    This class is testing the correct behavior of this partitioning column.
    """

    def setUp(self):

        super(TestCorrectionPartition, self).setUp()

        self.static_calculator = code.GlassboxBasedActiveTimeCalculator()

        self.source_schema = StructType(
            required_glassbox_event_fields_schema.fields
            + [struct_field__client_server_diff__online]
        )

        self.expected_schema = StructType(
            self.source_schema.fields
            + [struct_field__correction_partition_value]
        )

    def test_set_corection_value__1(self):
        """
        Testing the correction factor calculation for the case when User does the following sequence:
            - online
            - offline
            - online
        """

        # This is the expected DataFrame.
        # Note: source DataFrame is also created from this by
        # not selecting the FIELD__CORRECTION_PARTITION_VALUE column.
        source_json = [

            # user_1 - 0 sec diff - ONLINE
            '{"user__user_id": 1, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:01.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 01:00:01.001", "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CORRECTION_PARTITION_VALUE + '": 1}',
            # user_1 - 0 sec - OFFLINE
            '{"user__user_id": 1, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:02.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 01:00:02.001", "' + code.FIELD__NETWORK_STATUS + '": "OFFLINE", "' + code.FIELD__CORRECTION_PARTITION_VALUE + '": 1}',
            # user_1 - 0 sec - ONLINE
            '{"user__user_id": 1, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:03.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 01:00:03.001", "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CORRECTION_PARTITION_VALUE + '": 2}',

            # user_2 - 0 sec diff - ONLINE
            '{"user__user_id": 2, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:01.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 01:00:01.001", "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CORRECTION_PARTITION_VALUE + '": 1}',
            # user_2 - 0 sec - OFFLINE
            '{"user__user_id": 2, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:02.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 01:00:02.001", "' + code.FIELD__NETWORK_STATUS + '": "OFFLINE", "' + code.FIELD__CORRECTION_PARTITION_VALUE + '": 1}',
            # user_2 - 0 sec - ONLINE
            '{"user__user_id": 2, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:03.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 01:00:03.001", "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CORRECTION_PARTITION_VALUE + '": 2}',

            ]

        source_df = self.sqlCtx.read.json(
            self.sc.parallelize(source_json),
            schema=self.source_schema
        )

        expected_df = self.sqlCtx.read.json(
            self.sc.parallelize(source_json),
            schema=self.expected_schema
        )

        result_df = self.static_calculator.set_time_diff_correction_partition_value(source_df)

        self.assertDataFrameEqual(expected_df, result_df)

    def test_set_corection_value__2(self):
        """
        Testing the correction factor calculation for the case when User does the following sequence:
            - offline
            - offline
            - online
        """

        # This is the expected DataFrame.
        # Note: source DataFrame is also created from this by
        # not selecting the FIELD__CORRECTION_PARTITION_VALUE column.
        source_json = [

            # user_1 - 0 sec diff - ONLINE
            '{"user__user_id": 1, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:01.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 01:00:01.001", "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": 0, "' + code.FIELD__CORRECTION_PARTITION_VALUE + '": 1}',
            # user_1 - 0 sec - OFFLINE
            '{"user__user_id": 1, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:02.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 01:00:02.001", "' + code.FIELD__NETWORK_STATUS + '": "OFFLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": null, "' + code.FIELD__CORRECTION_PARTITION_VALUE + '": 1}',
            # user_1 - 0 sec - ONLINE
            '{"user__user_id": 1, "' + code.FIELD__PRODUCT_NAME + '": "Next", "' + code.FIELD__CLIENT_TIME + '": "2019-05-13 01:00:03.000", "' + code.FIELD__SERVER_TIME + '":"2019-05-13 01:00:03.001", "' + code.FIELD__NETWORK_STATUS + '": "ONLINE", "' + code.FIELD__CLIENT_SERVER_DIFF__ONLINE + '": 0, "' + code.FIELD__CORRECTION_PARTITION_VALUE + '": 2}',
        ]

if __name__ == "__main__":

    unittest2.main()
