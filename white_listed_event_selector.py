from pyspark.sql.functions import col


EVENT_TYPE = "core__event_type"


class WhiteListedEventSelector:
    """ This class that takes a DataFrame containing all the source events on creation and caches it in Spark memory.
    Then the `select_events_in_white_list(events)` method can be called repeatedly to assort the data into different
    groups uf `usage_types` (white lists). """

    def __init__(self, df):

        self.df = df.cache()

    def select_events_in_white_list(self, events):

        return self.df.where(col(EVENT_TYPE).isin(events))
