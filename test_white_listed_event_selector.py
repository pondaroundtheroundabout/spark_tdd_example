import random
import unittest2
from sparktestingbase.sqltestcase import SQLTestCase

import white_listed_event_selector as code


def collect_distinct_event_types_to_set(df):
    """ Selects the distinct event types form the DataFrame and collects the results into a Set. """

    return set(
        [str(row[code.EVENT_TYPE]) for row in df.select(code.EVENT_TYPE).distinct().collect()]
    )


class TestWhiteListedEventSelector(SQLTestCase):

    def assert_all_result_in_list(self, result_types, white_list):
        """ Make sure all the result types are in the white list. """

        self.assertEqual(0, len(result_types - white_list))

    def assert_no_result_in_list(self, result_types, black_list):
        """ Make sure none of the result types are in the black list. """

        self.assertEqual(len(black_list), len(black_list - result_types))

    def setUp(self):

        super(TestWhiteListedEventSelector, self).setUp()

        # Note: the list was crafted by running a distinct event type selection on the source data
        # and changing it's format to make it a list
        self.all_event_types = [
            "OpenAnalytics", "SelectBackgroundImage", "ChangeCreditCard", "OpenCommentThread",
            "OpenNeedHelpWidgetTopic", "ModifiedObject", "NewObject", "LoadedViewer", "OpenFolderListForPresentation",
            "ConnectSocialAccount", "OpenCreateAnalyticsLinkDialog", "HighlightText", "LoadedDashboard",
            "ShowedSuggestionsForMisspelledWord", "ModifiedPresenterNote", "OpenPreziCard", "DeactivatedViewLink",
            "EditComment", "ConnectPreziRemoteFailed", "CloseVideoSidebar", "ClosePrivacyDialog", "OpenViewLinkDialog",
            "ChangeLanguage", "DownloadApp", "OpenAccountSettings", "OpenPreziOptionsDialog", "OpenHelpMenu",
            "ConfirmTeamMemberRemoval", "SetPresentationColor", "AutoZoom", "ChangedComment", "CloseChartSidebar",
            "OpenAnalyticsLinkSettings", "ClickHamburgerMenuItem", "AddFolderCollaborator", "OpenedTopic",
            "LoadedDownloadAppDialog", "UpdateStylePreset", "LoadedLivePreziDialog", "OpenCompanyLeaderboard",
            "AddFolderCollaboratorFailed", "OpenUploadImageDialog", "LoadedLivePreziFollowPage", "LoadedPreziCard",
            "LoadedExplorePage", "CopyPresentation", "TeamMemberInvitationFailed", "RevertBackgroundImage",
            "SubmitPaymentInfo", "SelectedObject", "LoggedIn", "EndCommentThread", "ClickContextMenu",
            "OpenPathSidebar", "ExitArf", "CloseLineArrowSidebar", "UseWebTemplate", "CreateNewPresentation",
            "SavePresentationColorScheme", "LoadedEditor", "NavigatePrezi", "ReplaceMisspelledWord",
            "ExplorePresentations", "CopyObject", "ShowPdfInsertOptions", "InsertPptSlide", "RemovedComment"
        ]

        self.source_df = self.sqlCtx.read.orc("production_input/*")

        self.event_selector = code.WhiteListedEventSelector(self.source_df)

    def tearDown(self):

        super(TestWhiteListedEventSelector, self).tearDown()

        self.source_df.unpersist()

    def test_select_events_in_white_list(self):
        """ Make sure we select all the events that have event type on the white list. Not less, not more. """

        """ Assemble white and black list. """

        # Create a white list by taking a 20% random sample of the entire event type set
        white_list = set(random.sample(self.all_event_types, k=int(round(len(self.all_event_types) * 0.2))))

        # Create black list by removing the white list from all the event types
        black_list = set(self.all_event_types) - set(white_list)

        # Make sure the white and black list don't intersect and they make up all events
        self.assertEqual(0, len(self.all_event_types) - len(white_list) - len(black_list))

        """ Run the code to select the events that are on the white list 
        and use it to create the DataFrame containing the data that was filtered out. """

        result_df = self.event_selector.select_events_in_white_list(white_list)
        black_listed_df = self.source_df.subtract(result_df)

        # Make sure the result and the data that was filtered out have no intersection
        # and they add up to the original source data.
        self.assertEqual(0, self.source_df.count() - result_df.count() - black_listed_df.count())

        """Collect the distinct event types (for both white and black listed data) and make sure they are not empty. """

        # Result
        distinct_event_types_in_result = collect_distinct_event_types_to_set(result_df)
        self.assertGreater(len(distinct_event_types_in_result), 0)

        # Data filtered out
        distinct_event_types_in_data_filtered_out = collect_distinct_event_types_to_set(black_listed_df)
        self.assertGreater(len(distinct_event_types_in_data_filtered_out), 0)

        """ Make sure we only got events whose type is in the white list. """

        self.assert_all_result_in_list(distinct_event_types_in_result, white_list)
        self.assert_no_result_in_list(distinct_event_types_in_result, black_list)

        """ Make sure we have not filtered out any event which is on the white list. """

        self.assert_all_result_in_list(distinct_event_types_in_data_filtered_out, black_list)
        self.assert_no_result_in_list(distinct_event_types_in_data_filtered_out, white_list)


if __name__ == "__main__":

    unittest2.main()
