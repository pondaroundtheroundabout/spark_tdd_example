from pyspark.sql.functions import lit, when, unix_timestamp, count
from pyspark.sql.utils import AnalysisException
from pyspark.sql.window import Window


FIELD__USER_ID = "user__user_id"
FIELD__CLIENT_TIME = "core__client_time"
FIELD__SERVER_TIME = "core__server_time"
FIELD__NETWORK_STATUS = "platform__network_status"
FIELD__PRODUCT_NAME = "product_name"
FIELD__CLIENT_SERVER_DIFF__ALL = "client_server_time_diff_sec"
FIELD__CLIENT_SERVER_DIFF__ONLINE = "client_server_time_diff_sec_online"
FIELD__CORRECTION_PARTITION_VALUE = "correction_partition_value"

DEFAULT_VALUE__PRODUCT_NAME = "Next"


class GlassboxBasedActiveTimeCalculator:

    @staticmethod
    def select_required_columns(glassbox_events_df):
        """ Satisfying principle: select early. """

        return glassbox_events_df.select(
            FIELD__USER_ID, FIELD__CLIENT_TIME, FIELD__SERVER_TIME, FIELD__NETWORK_STATUS, FIELD__PRODUCT_NAME
        )

    @staticmethod
    def handle_default_product_name(df, default_value=DEFAULT_VALUE__PRODUCT_NAME):
        """ Historic data doesn't have `product_name` field and future data will have it.

        This function takes care of it by
            - adding the column with default value in case it doesn't exist
            - setting the value to default in case the column exists but it's null
            - passing the value if the column exists and contains data
        """

        try:
            df[FIELD__PRODUCT_NAME]
            # Do nothing else, column exists

        except AnalysisException:

            # column doesn't exist, let's add it with default value
            df = df.withColumn(FIELD__PRODUCT_NAME, lit(default_value))

        # Set temporary column to 'Next' in case of blank value
        df = df.withColumn(
            "tmp_product_name_blank_to_default",
            when(
                df[FIELD__PRODUCT_NAME].isNull(), lit(default_value)
            ).otherwise(
                df[FIELD__PRODUCT_NAME]
            )
        )

        # Replace temporary column and rename it to `product_name`
        return df.drop(FIELD__PRODUCT_NAME).withColumnRenamed("tmp_product_name_blank_to_default", FIELD__PRODUCT_NAME)

    def calculate_client_and_server_time_diff_in_seconds(self, df=None):
        """ Adds a column (FIELD__CLIENT_SERVER_DIFF__ALL) that contains the time difference between client and server time.
        The result is in seconds.
        The order of fields in the subtraction: CLIENT_TIME - SERVER_TIME.

        The function can be called as static method by passing in a DataFrame as parameter.
        Or it can be called on an initialized object without passing the parameter. """

        if not df:
            df = self.required_columns_df

        return df.withColumn(
            FIELD__CLIENT_SERVER_DIFF__ALL,
            unix_timestamp(df[FIELD__CLIENT_TIME]) - unix_timestamp(df[FIELD__SERVER_TIME])
        )

    def set_client_and_server_time_diff_for_online_rows(self, df=None):
        """ Sets FIELD__CLIENT_SERVER_DIFF__ONLINE to the value of FIELD__CLIENT_SERVER_DIFF__ALL
        in case the NETWORK_STATUS is not 'OFFLINE'. Otherwise the field is set to 'Null'.

        The function can be called as static method by passing in a DataFrame as parameter.
        Or it can be called on an initialized object without passing the parameter. """

        if not df:
            df = self.required_columns_df

        return df.withColumn(
            FIELD__CLIENT_SERVER_DIFF__ONLINE,
            when(df[FIELD__NETWORK_STATUS] == 'OFFLINE', lit(None)).otherwise(df[FIELD__CLIENT_SERVER_DIFF__ALL])
        )

    def set_time_diff_correction_partition_value(self, df=None):
        """ Adds FIELD__CORRECTION_VALUE column with the value with the last seen ONLINE (or Null) time difference.
        This is used to mitigate the issue caused by setting clock to incorrect value on client devices. """

        if not df:
            df = self.required_columns_df

        # SQL code below is taken from https://zeppelin.prezi.com/#/notebook/2EP3KEVF2 (created by @Z)
        # count(sva.diff_mill_online) over (
        #   partition by sva.user__user_id order by sva.core__server_time, sva.core__client_time
        #   rows between unbounded preceding and current row
        # )

        window = Window.partitionBy(
            df[FIELD__USER_ID]
        ).orderBy(
            df[FIELD__SERVER_TIME], df[FIELD__CLIENT_TIME]
        ).rowsBetween(
            Window.unboundedPreceding, Window.currentRow
        )

        return df.withColumn(
            FIELD__CORRECTION_PARTITION_VALUE,
            count(df[FIELD__CLIENT_SERVER_DIFF__ONLINE]).over(window)
        )

    def __init__(self, glassbox_events_df=None):
        """ Initializes the `required_columns_df` attribute to a DataFrame that contains only the required columns
        and where 'Null' `prduct_name` fields are set to default 'Next'.

        Metadata collected:
            `ct__source_rows` - count of source rows
        """

        if glassbox_events_df:

            # Select the required columns only and cache it
            self.required_columns_df = self.select_required_columns(
                self.handle_default_product_name(glassbox_events_df)
            )

            # Save count of source data
            self.ct__source_rows = self.required_columns_df.cache().count()

            # Create window
            self.window = Window.partitionBy(
                self.required_columns_df[FIELD__USER_ID], self.required_columns_df[FIELD__PRODUCT_NAME]
            ).orderBy(
                self.required_columns_df[FIELD__SERVER_TIME]
            )

        else:

            # Default (empty) constructor to be called for static methods
            pass
